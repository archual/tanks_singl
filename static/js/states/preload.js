"use strict"

let preload = function(game){}

preload.prototype = {
	preload: function(){ 
    let loadingBar = this.add.sprite(GAME_WIDTH / 2, 240, "loading");
    loadingBar.anchor.setTo(0.5,0.5);
    this.load.setPreloadSprite(loadingBar);
    
    this.game.load.image('play', 'static/img/assets/play.png');
    
		this.game.load.atlas('tank', 'static/img/spritesheets/tanks.png', 'static/img/spritesheets/tanks.json');
    this.game.load.atlas('enemy', 'static/img/spritesheets/enemy-tanks.png', 'static/img/spritesheets/tanks.json');
    
		this.game.load.atlas('hamster', 'static/img/spritesheets/sprites.png', 'static/img/spritesheets/sprites.json');
    
    this.game.load.image('logo', 'static/img/assets/logo.png');
    this.game.load.image('bullet', 'static/img/assets/bullet.png');
    this.game.load.image('earth', 'static/img/assets/scorched_earth.png');
    this.game.load.image('board', 'static/img/assets/background4.png');
    this.game.load.spritesheet('kaboom', 'static/img/spritesheets/explosion.png', 64, 64, 23);
   
		this.game.load.image("gameover", "static/img/assets/gameover.png");
	},
  	create: function(){
		this.game.state.start("GameTitle");
	}
}