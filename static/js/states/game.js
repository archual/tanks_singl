// @TODO Replace this code with tank game code.

"use strict"

let TankGame = function(game) {
}

TankGame.prototype = {

	// Variables.
	land: undefined,

	shadow: undefined,
	tank: undefined,
	turret: undefined,
		
	enemies: undefined,
	enemyBullets: undefined,
	enemiesTotal: 0,
	enemiesAlive: 0,
	explosions: undefined,
		
	currentSpeed: 0,
	cursors: undefined,
		
	bullets: undefined,
	fireRate: 100,
	nextFire: 0,
	
	create: function() {
		//  Resize our game world to be a 2000 x 2000 square
    this.game.world.setBounds(0, 0, 700, 600);

    //  Our tiled scrolling background
    this.land = this.game.add.tileSprite(0, 0, 700, 600, 'board');
    this.land.fixedToCamera = true;

    //  The base of our tank
    this.tank = this.game.add.sprite(26, 75, 'hamster', 'b_hamster_2.png');
    this.tank.anchor.setTo(0.5, 0.5);
    this.tank.animations.add('walk', ['b_hamster_2.png', 'b_hamster_1.png'], 5, true);

    //  This will force it to decelerate and limit its speed
    this.game.physics.enable(this.tank, Phaser.Physics.ARCADE);
    this.tank.body.drag.set(0.2);
    this.tank.body.maxVelocity.setTo(400, 400);
    this.tank.body.collideWorldBounds = true;

    //  Finally the turret that we place on-top of the tank body
    // this.turret = this.game.add.sprite(0, 0, 'tank', 'turret');
    // this.turret.anchor.setTo(0.3, 0.5);

    //  The enemies bullet group
    this.enemyBullets = this.game.add.group();
    this.enemyBullets.enableBody = true;
    this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.enemyBullets.createMultiple(100, 'bullet');
    
    this.enemyBullets.setAll('anchor.x', 0.5);
    this.enemyBullets.setAll('anchor.y', 0.5);
    this.enemyBullets.setAll('outOfBoundsKill', true);
    this.enemyBullets.setAll('checkWorldBounds', true);

    //  Create some baddies to waste :)
    this.enemies = [];

    this.enemiesTotal = 0;
    this.enemiesAlive = 0;

    for (let i = 0; i < this.enemiesTotal; i++)
    {
        this.enemies.push(new EnemyTank(i, this.game, this.tank, this.enemyBullets));
    }

    //  A shadow below our tank
    this.shadow = this.game.add.sprite(26, 75, 'hamster', 'shadow.png');
    this.shadow.anchor.setTo(0.5, 0.5);

    //  Our bullet group
    this.bullets = this.game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(30, 'bullet', 0, false);
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    //  Explosion pool
    this.explosions = this.game.add.group();

    for (let i = 0; i < 10; i++)
    {
        let explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }

    this.tank.bringToTop();
    // this.turret.bringToTop();

    this.game.camera.follow(this.tank);
    this.game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
    this.game.camera.focusOnXY(0, 0);

    this.cursors = this.game.input.keyboard.createCursorKeys();
	},
	
	update: function() {
		this.game.physics.arcade.overlap(this.enemyBullets, this.tank, this.bulletHitPlayer, null, this);

    this.enemiesAlive = 0;

    for (var i = 0; i < this.enemies.length; i++) {
      if (this.enemies[i].alive) {
        this.enemiesAlive++;
        this.game.physics.arcade.collide(this.tank, this.enemies[i].tank);
        this.game.physics.arcade.overlap(this.bullets, this.enemies[i].tank, this.bulletHitEnemy, null, this);
        this.enemies[i].update();
      }
    }

    if (this.cursors.left.isDown) {
      this.tank.angle -= 4;
    } else if (this.cursors.right.isDown) {
      this.tank.angle += 4;
    }

    if (this.cursors.up.isDown) {
      //  The speed we'll travel at
      this.currentSpeed = 300;
    } else {
      if (this.currentSpeed > 0) {
        this.currentSpeed -= 4;
        
        if (this.currentSpeed < 100) {
        	this.tank.animations.stop();
        }
      }
    }

    if (this.currentSpeed > 0) {
      this.game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.body.velocity);
      this.tank.animations.play('walk');
    }

    this.land.tilePosition.x = -this.game.camera.x;
    this.land.tilePosition.y = -this.game.camera.y;

    //  Position all the parts and align rotations
    this.shadow.x = this.tank.x;
    this.shadow.y = this.tank.y;
    this.shadow.rotation = this.tank.rotation;

    // this.turret.x = this.tank.x;
    // this.turret.y = this.tank.y;

    // this.turret.rotation = this.game.physics.arcade.angleToPointer(this.turret);

    if (this.game.input.activePointer.isDown) {
      //  Boom!
      this.fire();
    }
	},
	
	render: function() {
    // game.debug.text('Active Bullets: ' + bullets.countLiving() + ' / ' + bullets.length, 32, 32);
    this.game.debug.text('Enemies: ' + this.enemiesAlive + ' / ' + this.enemiesTotal, 32, 32);
	},
	
	bulletHitPlayer: function (tank, bullet) {
    bullet.kill();
	},
	
	bulletHitEnemy: function (tank, bullet) {
    bullet.kill();
    let destroyed = this.enemies[tank.name].damage();

    if (destroyed) {
      let explosionAnimation = this.explosions.getFirstExists(false);
      explosionAnimation.reset(tank.x, tank.y);
      explosionAnimation.play('kaboom', 30, false, true);
    }
	},
	
	fire: function () {
    if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0) {
        this.nextFire = this.game.time.now + this.fireRate;
        let bullet = this.bullets.getFirstExists(false);

        // bullet.reset(this.turret.x, this.turret.y);

        bullet.rotation = this.game.physics.arcade.moveToPointer(bullet, 1000, this.game.input.activePointer, 500);
    }
	}
}