// Main game file.
"use strict"

const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;

let game = new Phaser.Game(
    GAME_WIDTH, 
    GAME_HEIGHT, 
    Phaser.AUTO, 
    'game-container'
  );

game.state.add("Boot", boot);
game.state.add("Preload", preload);
game.state.add("GameTitle", gameTitle);
game.state.add("TheGame", TankGame);
game.state.add("GameOver", gameOver);
game.state.start("Boot");

